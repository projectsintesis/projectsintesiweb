import React from "react";
import type { DashboardSelection } from "@/components/Sidebar/Sidebar";
// eslint-disable-next-line import/extensions
import Sidebar from "@/components/Sidebar/Sidebar";
import Footer from "@/components/Footer/Footer";
import Header from "@/components/Header/Header"

interface Props {
  children: React.JSX.Element;
  selection: DashboardSelection;
}

export default function DashboardLayout({ selection, children }: Props) {
  return (
    <div className="h-full w-full flex items-stretch flex-nowrap">
      {/* 侧边 navbar */}
      <div className="absolute w-full"><Header /></div>

      <div className="mt-14">
        <Sidebar selectedIndex={selection} />
      </div>

      <div
        className="grow mt-14 bg-white w-full flex-nowrap overflow-auto"
      >
        {/* children内容 */}
        <div className="mt-20 w-full flex-nowrap px-20 box-border relative">
          {children}
        </div>
        <Footer />
      </div>
    </div>
  );
}