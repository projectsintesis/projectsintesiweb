import * as React from 'react';
import AspectRatio from '@mui/joy/AspectRatio';
import Button from '@mui/joy/Button';
import Card from '@mui/joy/Card';
import CardContent from '@mui/joy/CardContent';
import IconButton from '@mui/joy/IconButton';
import Typography from '@mui/joy/Typography';
import BookmarkAdd from '@mui/icons-material/BookmarkAddOutlined';  

function ProductCard ()
{
    return (
        <div
          className=''
        >
          <Card
            sx={{ width: 290, marginY: "20px" }}>
            <div>
              <Typography level="title-lg">Product Name</Typography>
              <Typography level="body-sm">Material</Typography>
              <IconButton
                aria-label="bookmark Bahamas Islands"
                variant="plain"
                color="neutral"
                size="sm"
                sx={{ position: 'absolute', top: '0.875rem', right: '0.5rem' }}
              >
                <BookmarkAdd />
              </IconButton>
            </div>
            <AspectRatio minHeight="120px" maxHeight="200px">
              <img
                src="https://cdn-image02.casetify.com/usr/3222/29043222/~v11/25961418x2_iphone-14__color_black_16004881.png.1000x1000-w.m80.webp"
                loading="lazy"
                alt="product image"
              />
            </AspectRatio>
            <CardContent orientation="horizontal">
              <div>
                <Typography level="body-xs">Preu total:</Typography>
                <Typography fontSize="lg" fontWeight="lg">
                  $2,900
                </Typography>
              </div>
              <Button
                  variant="outlined"
                  color="primary"
                  sx={{
                      borderColor: 'primary.500',
                      ml: 'auto',
                      alignSelf: 'center',
                      fontWeight: 600
                  }}
              >
                  Compra
              </Button>
            </CardContent>
          </Card>
        </div>
      );
}

export default ProductCard;