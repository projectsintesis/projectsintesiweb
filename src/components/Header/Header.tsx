import * as React from 'react';
import { useRouter } from "next/router";
import {
    IconButton,
    Button,
} from '@mui/material';

import {
    ShoppingCartOutlined,
    NotificationsOutlined,
    OpenInNewOutlined,
} from '@mui/icons-material';

function Header (){
    const router = useRouter();

    async function goToRoute(route: string) {
        await router.push(route);
    }

    return (
        <div className="w-full h-14 bg-white flex justify-start border-b-2 border-black items-center">
            <div className="w-5/6 h-full flex items-center">
                <h1
                    className="flex-1 ml-3 text-3xl cursor-pointer"
                    style={{fontFamily: "times new roman"}} 
                    onClick={async () => goToRoute("/")}
                >Change Case</h1>
            </div>
            <div 
                className="flex-1 justify-end h-full"
            >
                <div className=" w-5/6 flex justify-end items-center h-full">
                    <IconButton>
                        <ShoppingCartOutlined
                            className='text-gray-600 text-[28px]'
                        />
                    </IconButton>
                    <IconButton>
                        <NotificationsOutlined
                            className='text-gray-600 text-[28px]'
                        />
                    </IconButton>
                    <div className='pl-5 w-full flex justify-end'>
                        <Button
                            className='flex flex-1 gap-1 border-red-500 text-red-500 h-4/6 w-3/5 hover:bg-[#ef4444] hover:text-white hover:border-red-500'
                            variant="outlined"
                        >
                            <OpenInNewOutlined />
                            BLOG
                        </Button>                        
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Header;