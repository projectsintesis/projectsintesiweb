import React, { useState, useContext } from "react";
import { useRouter } from "next/router";
import {
    LocationOnOutlined,
    PhoneAndroidOutlined,
    QueryBuilderOutlined,
    WhatsApp,
    Instagram,
    Facebook,
    X,
} from "@mui/icons-material";
import Image from "next/image";
import Logo from "../../images/logo.png"  

const menuItems = [
    {
      text: "Inici",
      route: "/",
    },
    {
      text: "Producte",
      route: "/dashboard/product",
    },
    {
      text: "Sobre Nosaltres",
      route: "/dashboard/about",
    },
    {
      text: "Contacte",
      route: "/dashboard/contact",
    },
  ];
  

function Footer (){
    const router = useRouter();
    const [, setSelectedRoute] = useState(router.pathname);

    async function goToRoute(route: string) {
        await router.push(route);
        setSelectedRoute(route);
    }

    return (
        <div className="mt-20 w-full h-80 bg-black flex justify-center">
            <div className="w-4/5 bg-black flex justify-center">
                <Image
                    src={Logo} 
                    alt="Logo" 
                    className="flex-none w-48 h-72 self-center"
                    objectFit="cover"
                />
                <div className="flex mr-3">
                    <div className="mt-10 ml-20 side flex-1 text-white font-serif text-base leading-9">
                        <h1 className="mb-5 underline text-2xl">Paginas</h1>

                        {menuItems.map((item, index) => (
                            // eslint-disable-next-line react/no-array-index-key
                            <React.Fragment key={index}>
                                <p 
                                    onClick={async () => goToRoute(item.route)}
                                    className="hover:underline cursor-pointer"
                                >
                                    {item.text}
                                </p>
                            </React.Fragment>
                        ))}
                    </div>
                </div>

                <div className="flex-1">
                    <div className="mt-10 ml-20 side text-white text-base leading-10">
                        <div className="flex items-center mt-3 cursor-pointer">
                            <LocationOnOutlined className="mr-2" sx={{ fontSize: 35, color: "white" }} />
                            <h1>Barcelona</h1>
                        </div>
                        <div className="flex items-center mt-3">
                            <PhoneAndroidOutlined className="mr-2" sx={{ fontSize: 35, color: "white" }} />
                            <h1>123456789</h1>
                        </div>
                        <div className="flex items-center mt-3">
                            <QueryBuilderOutlined className="mr-2" sx={{ fontSize: 35, color: "white" }} />
                            <div className="leading-7">
                                <h1>Viernes hasta Miercoles |</h1>
                                <h1>11:30 - 21:30</h1>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="flex-1">
                    <div className="mt-10 ml-20 side text-white text-lg leading-10">
                        <div className="flex items-center mt-3 cursor-pointer">
                            <Instagram className="mr-2" sx={{ fontSize: 37, color: "white" }} />
                            <h1>Instagram</h1>
                        </div>
                        <div className="flex items-center mt-3 cursor-pointer">
                            <WhatsApp className="mr-2" sx={{ fontSize: 37, color: "white" }} />
                            <h1>WhatsApp</h1>
                        </div>
                        <div className="flex items-center mt-3 cursor-pointer">
                            <Facebook className="mr-2" sx={{ fontSize: 37, color: "white" }} />
                            <h1>Facebook</h1>
                        </div>
                        <div className="flex items-center mt-3 cursor-pointer">
                            <X className="mr-2" sx={{ fontSize: 37, color: "white" }} />
                            <h1>Twitter</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Footer;