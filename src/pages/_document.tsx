// pages/_document.tsx

import Document, { Html, Head, NextScript } from 'next/document';

class GlobalDocument extends Document {
  render() {
    return (
      <Html>
        <Head>
          {/* 引用网站图标 */}
          <link rel="icon" href="/favicon.png" />
        </Head>
        <body>
          <NextScript />
        </body>
      </Html>
    );
  }
}

export default GlobalDocument;
