import React from "react";
import Image from "next/image";
import Avatar from "@/images/avatar/Avatar.png"
import logoBlack from "@/images/logoBlack.png"
import { Divider } from "@mui/material";
import { DashboardSelection } from "@/components/Sidebar/Sidebar";
import DashboardLayout from "@/components/Layouts/DashboardLayout";

interface InfoItem {
  name: string;
  post: string;
  def: string;
}

const info: InfoItem[] = [
  {
    name: "Alex",
    post: "CEO",
    def: "definicio",
  },
  {
    name: "Shao",
    post: "Cap de qualitat",
    def: "definicio",
  },
  {
    name: "Angel",
    post: "Secretari",
    def: "definicio",
  },
  {
    name: "Carlos",
    post: "Porta veu",
    def: "definicio",
  },
]

function ComponentForAbout({name, post, def}: InfoItem) {
  return (
    <div className="flex flex-wrap w-1/2 mt-10 mb-20">
      <Image
          src={Avatar} 
          alt="Avatar" 
          className="rounded-full w-[200px] border-[3px] border-black"
          objectFit="cover"
      />
      <div className="ml-10 pt-10 flex-1">
        <h1 className="text-3xl font-bold">{name}: {post}</h1>
        <div className="mt-5">{def}</div>
      </div>
    </div>
  );
}

function AboutWePage() {
  return (
    <DashboardLayout selection={DashboardSelection.AboutWe}>
      <main>
        <div className="text-7xl font-black text-gray-800 mb-20">
          Sobre
          <br />  
          Nosaltres
        </div>

        <div className="flex">
          <div className="w-1/3 flex justify-center">
            <Image
                src={logoBlack}
                alt="logoBlack" 
                className="flex-none w-44 h-72 self-center"
                objectFit="cover"
            />
          </div>
          <div className="w-3/5 flex">
            <Divider className="bg-black mr-5" orientation="vertical" flexItem />
            <div className="text-wrap text-base leading-8 pr-5">Oferim un producte que s’utilitza diàriament. Tenir una funda personalitzada per a qualsevol mòbil. Podràs canviar el fons de la teva funda i també podràs canviar les tires de goma que aguanten el mòbil. També podràs personalitzar els botons de pujar i baixar el volum. En la nostra empresa podem fer dissenys a mesura dels requisits del client. <br /><br />També produirem les gomes de cada dispositiu i que els clients puguin personalitzar-ho al màxim. Aquesta empresa serà de materials reciclats i també aprofitem per no llençar tants plàstics al medi ambient, reutilitzarem les gomes del mòbil, els fons del mòbil i els botons perquè sigui 100% reutilitzable. Doncs podràs treure la banda de goma i poder-la canviar per un altre fons o una altra goma</div>
          </div>
        </div>

        <Divider className="mt-24 bg-black" />

        <div
         className="mt-2 flex w-full flex-wrap justify-center"
        >
          {info.map((item, index) => (
            // eslint-disable-next-line react/no-array-index-key
            <React.Fragment key={index}>
              <ComponentForAbout {...item} />
            </React.Fragment>
          ))}
        </div>
      </main>
    </DashboardLayout>
  );
}

export default AboutWePage;