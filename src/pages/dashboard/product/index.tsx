import React from "react";
import { DashboardSelection } from "@/components/Sidebar/Sidebar";
import DashboardLayout from "@/components/Layouts/DashboardLayout";
import ProductCard from "@/components/ProductCard/ProductCard";
import {
  Button,
  Divider,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  Pagination,
} from "@mui/material";

import {
  SearchOutlined,
} from '@mui/icons-material';

import {
  Input,
} from '@mui/joy';

function ProductsPage() {
  const [material, setMaterial] = React.useState('');
  const [phoneModel, setPhoneModel] = React.useState('');

  const handleChange = (event: any) => {
    setMaterial(event.target.value);
    setPhoneModel(event.target.value);
  };

  return (
    <DashboardLayout selection={DashboardSelection.Product}>
      <main>
        <div className="w-full flex flex-nowrap justify-center">
          <Input
            className="w-3/6 flex items-center border-[1px] border-gray-100 focus-within:bg-white bg-gray-50 hover:bg-white focus-within:border-cyan-500"
            placeholder="Troba el teu producte"
            startDecorator={<SearchOutlined className="ml-1 text-cyan-500 text-[28px]" />}
            endDecorator={<Button className="bg-cyan-500 text-white px-7 hover:bg-cyan-400">Cerca</Button>}
            sx={{
              fontSize: "18px",
              "--Input-focusedThickness": "0px",
              "--Input-minHeight": "50px",
              "--Input-decoratorChildHeight": "32px",
              "--Input-gap": "5px",
              "--Input-radius": "5px"
            }}
          />
        </div>

        <div className="mt-8 relative flex">
          <FormControl sx={{ m: 1, minWidth: 120 }} size="small">
            <InputLabel id="demo-select-small-label">Material</InputLabel>
            <Select
              labelId="demo-select-small-label"
              id="demo-select-small"
              value={material}
              label="Material"
              onChange={handleChange}
            >
              <MenuItem value="">
                <em>Auto</em>
              </MenuItem>
              <MenuItem value={"Silicona"}>Silicona</MenuItem>
              <MenuItem value={"Plastic"}>Plastic</MenuItem>
              <MenuItem value={"Alumini"}>Alumini</MenuItem>
            </Select>
          </FormControl>
          <FormControl sx={{ m: 1, minWidth: 120 }} size="small">
            <InputLabel id="demo-select-small-label">Model</InputLabel>
            <Select
              labelId="demo-select-small-label"
              id="demo-select-small"
              value={phoneModel}
              label="Model"
              onChange={handleChange}
            >
              <MenuItem value="">
                <em>Auto</em>
              </MenuItem>
              <MenuItem value={"OPPO"}>OPPO</MenuItem>
              <MenuItem value={"Red MI"}>Red MI</MenuItem>
              <MenuItem value={"HUAWEI"}>HUAWEI</MenuItem>
              <MenuItem value={"Apple"}>Apple</MenuItem>
              <MenuItem value={"Samsung"}>Samsung</MenuItem>
            </Select>
          </FormControl>
        </div>

        <Divider className="mt-5 bg-gray-400" />
        <div className="grid gap-10 grid-cols-grid-auto-fill justify-center mt-10">
          <ProductCard /><ProductCard /><ProductCard /><ProductCard /><ProductCard /><ProductCard /><ProductCard /><ProductCard /><ProductCard /><ProductCard /><ProductCard /><ProductCard /><ProductCard /><ProductCard /><ProductCard /><ProductCard /><ProductCard />
        </div>
        <div className="flex justify-end mt-10 mr-20">
          <Pagination count={10} size="large" />
        </div>
      </main>
    </DashboardLayout>
  );
}

export default ProductsPage;