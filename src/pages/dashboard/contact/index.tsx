import * as React from "react";
import { IMaskInput } from 'react-imask';
import { DashboardSelection } from "@/components/Sidebar/Sidebar";
import DashboardLayout from "@/components/Layouts/DashboardLayout";
import { Divider, Button } from "@mui/material";
import {
  WhatsApp,
  Instagram,
  MailOutline,
  Call,
  Facebook,
  X,
  LocationOnOutlined,
} from "@mui/icons-material";

import {
  Input,
  Textarea,
  Typography,
} from '@mui/joy';

interface CustomProps {
  onChange: (event: { target: { name: string; value: string } }) => void;
  name: string;
}

const MaskInput = React.forwardRef<HTMLElement, CustomProps>(
  function TextMaskAdapter(props) {
    const { onChange, ...other } = props;
    return (
      <IMaskInput
        {...other}
        mask="(+34) 000 00 00 00"
        definitions={{
          '#': /[1-9]/,
        }}
        onAccept={(value: any) => onChange({ target: { name: props.name, value } })}
        overwrite
      />
    );
  },
);


function ContactPage() {
  const [value, setValue] = React.useState('(+34) ');
  const [text, setText] = React.useState('');

  return (
    <DashboardLayout selection={DashboardSelection.Contact}>
      <main>
        <div className="text-7xl font-black text-gray-800 mb-20">Contacte</div>
        <div
          className="flex gap-14 flex-wrap mb-44"
        >
          <div className="w-2/6">
            <div className="flex items-center mt-2 cursor-pointer gap-3">
                <WhatsApp className="mr-2" sx={{ fontSize: 35, color: "black" }} />
                <div className="mt-3">
                  <h1 className="text-2xl">WhatsApp</h1>
                  <h1 className="mt-2 text-gray-500">+34 123456789</h1>
                </div>
            </div>
            <div className="flex items-center mt-2 cursor-pointer gap-3">
                <MailOutline className="mr-2" sx={{ fontSize: 35, color: "black" }} />
                <div className="mt-3">
                  <h1 className="text-2xl">Email</h1>
                  <h1 className="mt-2 text-gray-500">changecase@gmail.com</h1>
                </div>
            </div>
            <div className="flex items-center mt-2 cursor-pointer gap-3">
                <LocationOnOutlined className="mr-2" sx={{ fontSize: 35, color: "black" }} />
                <div className="mt-3">
                  <h1 className="text-2xl">Direccio</h1>
                  <h1 className="mt-2 text-gray-500">Barcelona</h1>
                </div>
            </div>
            <Divider className="bg-black mt-5"/>
            <div className="flex items-center mt-5 mr-10 cursor-pointer gap-8 justify-end">
                <Instagram sx={{ fontSize: 25, color: "black" }} />
                <Facebook sx={{ fontSize: 25, color: "black" }} />
                <X sx={{ fontSize: 25, color: "black" }} />
            </div>
          </div>
          <div className="flex-1 flex gap-8 flex-col">
            <div className="flex items-center">
              <Input
                className="w-full border-r-0 flex items-center border-[1px] border-black focus-within:bg-white bg-gray-50 hover:bg-white focus-within:border-black"
                placeholder="Nom"
                sx={{
                  fontSize: "18px",
                  borderTopRightRadius: "0px",
                  borderBottomRightRadius: "0px",
                  "--Input-focusedThickness": "0px",
                  "--Input-minHeight": "50px",
                  "--Input-decoratorChildHeight": "32px",
                  "--Input-gap": "15px",
                  "--Input-radius": "5px"
                }}
              />
              <Input
                className="w-full flex items-center border-[1px] border-black focus-within:bg-white bg-gray-50 hover:bg-white focus-within:border-black"
                placeholder="Cognom"
                sx={{
                  fontSize: "18px",
                  borderTopLeftRadius: "0px",
                  borderBottomLeftRadius: "0px",
                  "--Input-focusedThickness": "0px",
                  "--Input-minHeight": "50px",
                  "--Input-decoratorChildHeight": "32px",
                  "--Input-gap": "15px",
                  "--Input-radius": "5px"
                }}
              />
            </div>
            <div className="flex items-center">
              <Input
                className="w-8/12 border-r-0 flex flex-1 items-center border-[1px] border-black focus-within:bg-white bg-gray-50 hover:bg-white focus-within:border-black"
                placeholder="Correu electrònic"
                startDecorator={
                  <MailOutline className="ml-1 text-black text-[28px]" />
                }
                sx={{
                  fontSize: "18px",
                  borderTopRightRadius: "0px",
                  borderBottomRightRadius: "0px",
                  "--Input-focusedThickness": "0px",
                  "--Input-minHeight": "50px",
                  "--Input-decoratorChildHeight": "32px",
                  "--Input-gap": "15px",
                  "--Input-radius": "5px"
                }}
              />
              <Input
                className="w-4/12 flex items-center border-[1px] border-black focus-within:bg-white bg-gray-50 hover:bg-white focus-within:border-black"
                placeholder="Número de telèfon"
                value={value}
                onChange={(event) => setValue(event.target.value)}
                slotProps={{ input: { component: MaskInput } }}
                startDecorator={
                  <Call className="ml-1 text-black text-[28px]" />
                }
                sx={{
                  fontSize: "18px",
                  borderTopLeftRadius: "0px",
                  borderBottomLeftRadius: "0px",
                  "--Input-focusedThickness": "0px",
                  "--Input-minHeight": "50px",
                  "--Input-decoratorChildHeight": "32px",
                  "--Input-gap": "15px",
                  "--Input-radius": "5px"
                }}
              />
            </div>
            <div className="flex-1">
              <Textarea
                className="text-[18px] h-full w-full flex justify-end border-[1px] border-black focus-within:bg-white bg-gray-50 hover:bg-white focus-within:border-black"
                placeholder="Messages"
                value={text}
                onChange={(event) => setText(event.target.value)}
                endDecorator={
                  <Typography level="body-xs" sx={{ ml: 'auto' }}>
                    {text.length} character(s)
                  </Typography>
                }
              />
              <div className="flex w-full justify-end h-[50px]">
                <Button
                  className="w-2/12 normal-case text-black mt-5 font-bold text-sm bg-yellow-400 h-full rounded-3xl hover:bg-yellow-500"
                  size="large"
                >
                  Enviar
                </Button>
              </div>
            </div>
          </div>
        </div>
      </main>
    </DashboardLayout>
  );
}

export default ContactPage;