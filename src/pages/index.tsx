import React from "react";
import Image from "next/image";
import logoCBlack from "@/images/logoCBlack.png"
import logoBlack from "@/images/logoBlack.png"

import Case from "@/images/sourceImage/Case.png"
import Mobile from "@/images/sourceImage/Mobile.png"
import Side from "@/images/sourceImage/Side.png"

import { Divider } from "@mui/material";
import { DashboardSelection } from "@/components/Sidebar/Sidebar";
import DashboardLayout from "@/components/Layouts/DashboardLayout";

function Home() {
  const Map = '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d95780.6676222969!2d2.0577883192910558!3d41.39263855807847!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x12a49816718e30e5%3A0x44b0fb3d4f47660a!2z5be05aGe572X57qz5be05aGe572X6YKj!5e0!3m2!1szh-CN!2ses!4v1708624104911!5m2!1szh-CN!2ses" width="100%" height="650" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>'

  return (
    <DashboardLayout selection={DashboardSelection.Home}>
      <main>
        <div className="flex mt-40 mb-24 items-center">
          <div className="flex-1 w-3/5 justify-center">
            <div className="ml-40 flex items-center">
              <Image
                src={logoCBlack}
                alt="logoCBlack"
                className="flex-none w-16 h-20 self-center"
                objectFit="cover"
              />
              <div className="text-8xl flex items-center" style={{fontFamily: "times new roman"}}>hange</div>
            </div>
            <div className="ml-80 flex items-center">
              <Image
                src={logoCBlack}
                alt="logoCBlack"
                className="flex-none w-16 h-20 self-center"
                objectFit="cover"
              />
              <div className="text-8xl flex items-center" style={{fontFamily: "times new roman"}}>ase</div>
            </div>
          </div>
          <div className="w-2/6 flex justify-start">
            <Image
              src={logoBlack}
              alt="logoBlack" 
              className="flex-none w-44 h-72 self-center"
              objectFit="cover"
            />
          </div>
        </div>

        <Divider className="bg-black" />

        <div className="mt-24 flex flex-nowrap justify-center gap-5 items-center">
          <div className="flex flex-col justify-center items-center w-1/3">
            <Image
              src={Mobile} 
              alt="Mobile" 
              className="w-[200px] h-[200px] rounded-[100%] border-[3px] border-black"
              style={{ objectFit: "cover" }}
            />
            <div className="font-bold mt-3 text-2xl" style={{ fontFamily: "times new roman" }} >Material</div>
            <div className="mt-5 w-9/12 text-center text-gray-600">Podem fer fundes per a mòbils de diferents materials segons les necessitats del client: plàstic, metall...</div>
          </div>
          <div className="flex flex-col justify-center items-center w-1/3">
            <Image
              src={Case} 
              alt="Case" 
              className="w-[200px] h-[200px] rounded-[100%] border-[3px] border-black"
              style={{ 
                objectFit: "cover",
                objectPosition: "0% 0%",
              }}
            />
            <div className="font-bold mt-3 text-2xl" style={{ fontFamily: "times new roman" }} >Model de telèfon</div>
            <div className="mt-5 w-9/12 text-wrap text-center text-gray-600">Podem personalitzar les fundes de telèfons mòbils segons diferents models de telèfons mòbils...</div>
          </div>
          <div className="flex flex-col justify-center items-center w-1/3">
            <Image
              src={Side} 
              alt="Side" 
              className="w-[200px] h-[200px] rounded-[100%] border-[3px] border-black"
              style={{ 
                objectFit: "cover",
                objectPosition: "100% 20%",
              }}
            />
            <div className="font-bold mt-3 text-2xl" style={{ fontFamily: "times new roman" }} >Botó personalitzat</div>
            <div className="mt-5 w-9/12 text-wrap text-center text-gray-600">Personalitzar els botons de pujar, baixar el volum i el botó d'encendre.</div>
          </div>
        </div>
        <div className="w-full mt-20" dangerouslySetInnerHTML={{ __html: Map }} />
      </main>
    </DashboardLayout>
  );
}

export default Home;